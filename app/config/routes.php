<?php
$router = new Phalcon\Mvc\Router(false);

$router->add("/", array(
    'controller' => 'index',
    'action' => 'index'
));

$router->add("/create", array(
    'controller' => 'index',
    'action' => 'create'
));

$router->handle();
return $router;