<?php

class IndexController extends ControllerBase
{
    public function initialize(){
        parent::initialize();


    }

    public function indexAction() {

//        $db = Db::getInstance();

        $posts = PostCollection::find([
            [
                'is_visible' => 1,
            ],
            "sort" => [
                'created_at' => -1,
            ],
        ]);

        $result = [];
        if (false !== $posts){
            $key = 0;
            foreach ($posts as $post){

                // Формируем информацию для отображенеия
                $result[$key]['post_id'] = $post->post_id;
                $result[$key]['title'] = $post->title;
                $result[$key]['date_create'] = $post->created_at;
                $result[$key]['author'] = $post->author;

//                $result[$key]['user_can_edit'] = ($author_id == $this->user_id);
                $key++;
            }
        }

        $this->view->setVar('posts',$result);
        $this->view->setVar('user_can_create',($this->user_login !== null));

    }

    /**
     * Экшен для просмотра поста
     */
    public function viewAction(){

        $get = $this->request->get();

        // Проверяем на наличие идентификатора поста
        if (empty($get['post'])){
            $this->response->redirect('/');
        }

        $post_id = trim($get['post']);

        // Проверка на наличие не позволенных символов
        if (!Helper_Util::validPostId($post_id)){
            $this->response->redirect('/');

        }

        $post = PostCollection::findFirst([
            ['post_id' => $post_id,],
        ]);

//        Log::getInstance()->info(__METHOD__." :: ".(json_encode($post)));

        if (false === $post){
            throw new Exception("Not found!");
        }

        $this->view->setVars([
            'post_id' => $post_id,
            'title' => $post->title,
            'date_create'=>$post->created_at,
            'body'=>nl2br($post->body),
            'author' => $post->author,
            'user_is_author'=>((int)($this->user_login === $post->author))
        ]);
    }

}

