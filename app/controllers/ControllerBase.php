<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    protected $user_login = null;

    public function initialize(){
        // CSS LIB
        $this->assets->addCss("/css/lib/bootstrap.min.css");

        // Общие стили для всего проекта ложим в main.css
        $this->assets->addCss("/css/main.css");

        // Проверка что пользователь прошел авторизацию
        if ($this->session->has("user_login")) {
            // Получение значения
            $this->user_login = $this->session->get("user_login");
            $this->view->setVar('user',$this->user_login);
        }


        // JS LIB
        $footerCollection = $this->assets->collection("footer");
        $footerCollection->addJs("/js/lib/jquery.min.js");
//        $this->assets->addJs("/js/lib/handlebars.js");
        $footerCollection->addJs("/js/lib/bootstrap.min.js");

        // JS
        $footerCollection->addJs("/js/functions.js");
    }

}
