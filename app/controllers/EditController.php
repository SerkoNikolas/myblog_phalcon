<?php

class EditController extends ControllerBase
{
    public function initialize(){
        parent::initialize();

        // Если пользователь не авторизирован - перенаправляем на титульную
        if (empty($this->user_login)){
            $this->response->redirect('/');
        }

        $footerCollection = $this->assets->collection("footer");
        $footerCollection->addJs("/js/layout/edit.js");

    }

    /**
     * Экшен применяется как для создания новго поста, так и для редактирования существующего
     */
    public function editAction(){

        // Значения по умолчанию
        $title = '';
        $body = '';
        $keyWords = '';
        $post_id = null;

        // Проверяем на наличие идентификатора поста
        $get = $this->request->get();
        if (!empty($get['post'])){
            $will_post_id = trim($get['post']);

            $post = $this->validPostAccess($will_post_id);
            if (false !== $post){
                $title = $post->title;
                $body = $post->body;
                $keyWords = $post->keyWords;
                $post_id = $will_post_id;
            }
        }

        $this->view->setVars([
            'title'=>$title,
            'body'=>$body,
            'keyWords'=>$keyWords,
            'post_id'=>$post_id,
            'header' =>(($post_id == null) ? 'Создание нового поста' : 'Редактирование поста'),
        ]);

    }

    /**
     * Экшен для создания новго или редактирования существующего поста
     */
    public function saveAction(){
        // Отключаем вывод стандартной вьюхи
        $this->view->disable();

        // Проверка что данные пришли методом POST
        if ($this->request->isPost()) {
            $myMess = '';
            // Получение POST данных
            $post = $this->request->getPost();

            $post_id = (!empty($post['post_id']) ? trim($post['post_id']) : '');
            $title = (!empty($post['title']) ? trim(htmlspecialchars(strip_tags($post['title']))) : '');
            $keyWords = (!empty($post['keyWords']) ? trim(htmlspecialchars(strip_tags($post['keyWords']))) : '');
            $body = (!empty($post['body']) ? trim(htmlspecialchars($post['body'])) : '');

            $create = true;
            $result = false;

            $msg = '';
            if (!$this->validateInputData($title,$keyWords,$body,$msg)){
                echo json_encode(array('status'=>'ERROR','message'=>$msg));
                die;
            }

            // Проверяем, на наличие идентификатора (обновление поста)
            if (!empty($post_id)){
                $post = $this->validPostAccess($post_id);

                if (false !== $post){
                    $post->title = $title;
                    $post->author = $this->user_login;
                    $post->keyWords = $keyWords;
                    $post->body = $body;
                    $post->options = [];
                    $post->comments = [];

                    $update_post_id = Helper_Util::transliterate($title).'_'.(time());
                    $post->post_id = $update_post_id;

                    $result = $post->save();

                    // В случае ошибки - получаем ее описание (Вивод ошибки в конце)
                    if (false === $result){
                        $myMess = "Не удалось обновить данные: \n";

                        $messages = $post->getMessages();

                        foreach ($messages as $message) {
                            $myMess .= $message. "\n";
                        }
                    } else {
                        // Нужно передать на JS для коректного перехода на страницу поста
                        $post_id = $update_post_id;
                    }
                } else {
                    $myMess = "Access denied!";
                }
            } else {
                // Заносим данные
                $model_blog = new PostCollection();

                // Генерируем новый идентиификатор
                $new_post_id = Helper_Util::transliterate($title).'_'.(time());

                $model_blog->post_id = $new_post_id;
                $model_blog->title = $title;
                $model_blog->author = $this->user_login;
                $model_blog->keyWords = $keyWords;
                $model_blog->body = $body;
                $model_blog->options = [];
                $model_blog->comments = [];
                $result = $model_blog->save();

                // В случае ошибки - получаем ее описание
                if (false !== $result){
                    // Нужно передать на JS для коректного перехода на страницу поста
                    $post_id = $new_post_id;
                } else {
                    $myMess = "Не удалось занести данные: \n";
                    $messages = $model_blog->getMessages();

                    foreach ($messages as $message) {
                        $myMess .= $message. "\n";
                    }
                }
            }

            if (false === $result) {
                echo json_encode(array('status'=>'ERROR','message'=>$myMess));

            } else {
                echo json_encode(['status'=>'SUCCESS','postId'=>$post_id]);
            }

        } else {
            echo 'bad request method!';
            die;
        }
    }

    /** Экшен для удаления информации о посте
     * @throws Exception
     */
    public function deleteAction(){
        // -- Блок непосредственного удаления данных
        if ($this->request->isPost()){
            // Отключаем вывод стандартной вьюхи
            $this->view->disable();
            $postData = $this->request->getPost();

            // Если отсутствует идентификатор поста - перенаправляем на титул
            if (empty($postData['post_id'])){
                echo json_encode(['status'=>'Неверные данные']);
                die;
            }


            $post = $this->validPostAccess($postData['post_id']);

            // Если есть доступ к данным - удаляем их
            if (false !== $post){
                $result = $post->delete();

                if (false === $result){
                    $myMess = '';
                    $messages = $post->getMessages();
                    foreach ($messages as $message) {
                        $myMess .= $message. "\n";
                    }
                    echo json_encode(['status'=>'ERROR','message'=>'Не удалось удалить пост. Попробуйте попоже']);
                    Log::getInstance()->info(__METHOD__." :: Ошибка удаления поста :: <".$postData['post_id']."> :: ".$myMess);
                    die;
                } else {
                    echo json_encode(['status'=>'SUCCESS']);
                    die;
                }
            }

            echo json_encode(['status'=>'ERROR']);
            die;
        }


        // -- Переспрашиваем, готов ли пользователь удалить данные
        // Проверяем на наличие идентификатора поста
        $get = $this->request->get();
        if (empty($get['post'])){
            $this->response->redirect('/');
        }

        $post_id = trim($get['post']);

        $post = $this->validPostAccess($post_id);
        if (false !== $post){
            $this->view->setVars([
                'title'=>$post->title,
                'post_id'=>$post_id,
            ]);
        } else {
            throw new Exception("Access denied!");
        }

    }

    /** Метод для проверки параметров перед записью в БД
     * @param string $title
     * @param string $keyWords
     * @param string $body
     * @param $msg
     * @return bool
     */
    private function validateInputData($title, $keyWords, $body, &$msg){
        $has_err = false;
        $msg = '';

        // Проверка, чтобы все поля были заполнены
        if (empty($title) || empty($keyWords) || empty($body)){
            $msg .= 'Не все поля заполнены';
            $has_err = true;
        }

        $patt = '/^[\w\s\d]{5,250}$/iu';

        // Проверка на разрешенные символы
        if (!preg_match($patt,$title)){
            $msg .= ($has_err ? "<br /> \n":'').'Название поста должно содержат только цыфры и буквы, а также быть от 5 до 250 символов!';
            $has_err = true;
        }

        // Проверка на разрешенные символы
        if (!preg_match($patt,$keyWords)){
            $msg .= ($has_err ? "<br /> \n":'').'Ключевые слова должны содержат только цыфры и буквы, а также быть от 5 до 250 символов!';
            $has_err = true;
        }

        return !$has_err;
    }

    /** Метод для проверки поста на возможность редактирования
     * если пост есть и мы имеем право редактировать - возвращаем обьект с записями
     * @param string $post_id
     * @return bool|mixed
     */
    private function validPostAccess($post_id){
        if (empty($post_id)){
            return false;
        }

        // Проверка на наличие не позволенных символов
        if (Helper_Util::validPostId($post_id)){
            $post = PostCollection::findFirst([
                [
                    'post_id' => $post_id,
                    'author' => $this->user_login,
                ],
            ]);

//            Log::getInstance()->info(__METHOD__." :: ".(json_encode($post)));

            return $post;
        }

        return false;
    }

}

