<?php

class AccountController extends ControllerBase
{
    public function initialize(){
        parent::initialize();

    }

    /**
     * Авторизация пользователя
     */
    public function loginAction() {

        $login = null;
        $message_err = '';

        /*$model_users = new UsersCollection();

        $model_users->login='user';
        $model_users->password=md5('qwerty');

        $model_users->save();*/

        if ($this->request->isPost()){
            $post = $this->request->getPost();

            $login = (!empty($post['login']) ? $post['login'] : null);
            $pass = (!empty($post['password']) ? $post['password'] : null);

            if (empty($login) || empty($pass)){
                $message_err = 'Все поля должны быть заполнены!';
            } else {

                // Поиск пользователя в базе
                $users = UsersCollection::findFirst([
                    [
                        'login' => $login,
                        'password' => md5($pass),
                    ]
                ]);

                // Если пользователь найден - добавляем информацию в сессию
                if (false !== $users){

                    // Обновляем время последнего входа в систему
                    try {
                        $users->last_login = date('Y-m-d H:i:s');
                        $users->save();
                    } catch (Exception $e){}

                    // Заносим информацию о пользователе в сессию
                    $this->session->set("user_login", $users->login);

                    // Переходим на список постов
                    $this->response->redirect('/');
                } else {
                    $message_err = 'Неверный логин или пароль';
                }
            }
        }

        $this->view->setVar('message',$message_err);
        $this->view->setVar('login',$login);

    }


    /**
     * Экшэн выхода пользователя с системы
     */
    public function logoutAction(){
        // Полная очистка сессии
        $this->session->destroy();
        $this->response->redirect('/account/login');
    }

}

