<?php

use Phalcon\Logger;
use Phalcon\Logger\Adapter\File as FileAdapter;

class Log {

    private static $instance;

    private $loger;

    public function __construct(){
        $date = date('Y-m-d');
        $this->logger = new FileAdapter(APP_PATH."/logs/{$date}.log");
    }

    /**
     * @param string $msg
     */
    public function info($msg){
        $this->logger->info($msg);
    }

    /** Singleton для Loghger'a
     * @return Log
     */
    public static function getInstance(){
        if (!self::$instance){
            self::$instance = new Log();
        }
        return self::$instance;
    }
}