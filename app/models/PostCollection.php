<?php
use Phalcon\Mvc\Collection;

class PostCollection extends Collection {
    /**
     * Идентификатор поста
     * @var string
     */
    public $post_id;
    /**
     * Автор поста
     * @var string
     */
    public $author;

    /**
     * Название поста
     * @var string
     */
    public $title;

    /**
     * Дата публикации поста
     * @var string - mongo type Date
     */
//    public $date;
    /**
     * Ключевые слова
     * @var mixed
     */
    public $keyWords;

    /**
     * Содержание поста
     * @var string
     */
    public $body;

    /**
     * Дополнительные опции
     * @var mixed
     */
    public $options;

    /**
     * Комментарии
     * @var mixed
     */
    public $comments;

    public $is_visible;

    public $created_at;
    public $modified_in;

    public function initialize(){
        $this->setSource("posts");
    }


    public function beforeCreate()
    {
        // Set the creation date
        $this->created_at = date("Y-m-d H:i:s");
        $this->is_visible = 1;
    }

    public function beforeUpdate()
    {
        // Set the modification date
        $this->modified_in = date("Y-m-d H:i:s");
    }
}