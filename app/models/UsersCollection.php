<?php
use Phalcon\Mvc\Collection;

class UsersCollection extends Collection {
    /**
     * Логин пользователя
     * @var string
     */
    public $login;

    /**
     * Пароль
     * @var string
     */
    public $password;


    /**
     * Зарегистрирован
     * @var string
     */
    public $created_at;

    /**
     * Последний раз входил
     * @var string
     */
    public $last_login;


    public function initialize(){
        $this->setSource("users");
    }


    public function beforeCreate()
    {
        // Set the creation date
        $this->created_at = date("Y-m-d H:i:s");
    }

    public function beforeUpdate()
    {
    }

}