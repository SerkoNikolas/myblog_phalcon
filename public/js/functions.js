/**
 * Created by serko on 11/6/16.
 */

function sendForm(url,data,callback){
    return $.ajax({
        url: url,
        dataType: 'json',
        processData: false,
        contentType: false,
        data: data,
        method: 'post',
        scriptCharset: 'utf-8',
        success: function (response) {
            if (callback){
                callback(response);
            }
        }
    });
}

var GMessages = (function(){
    return {
        info: function(msg){
            alert(msg);
        },
        confirm: function(msg){
            return confirm(msg);
        },
        warning: function(msg){
            alert(msg);
        }
    }
})();
