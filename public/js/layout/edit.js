/**
 * Created by serko on 11/6/16.
 */

// Создание и редактирование поста
$(function () {
    var $form = $('.js-createForm');

    if (!$form.length){
        return ;
    }

    // Находим поля на странице
    var $inPostId = $form.find('.js-post_id');
    var $inTitle = $form.find('.js-title');
    var $inKeyWords = $form.find('.js-keyWord');
    var $inBody = $form.find('.js-post-body');

    var $btnReset = $form.find('.js-reset');
    var $btnSubmit = $form.find('.js-submit');

    // Кнопка очистки содержимого
    $btnReset
        .off('click')
        .on('click',function(){
            if (GMessages.confirm('Вы действительно хотите очистить все поля?')){
                $inTitle.val('');
                $inKeyWords.val('');
                $inBody.val('');
            }
        });

    // Кнопка отправки данных на сервер
    $btnSubmit
        .off('click')
        .on('click',function(){
            var postId = ((typeof $inPostId != 'undefined') ? $inPostId.val().trim() : '');
            var title = ((typeof $inTitle != 'undefined') ? $inTitle.val().trim() : '');
            var keyWords = ((typeof $inKeyWords != 'undefined') ? $inKeyWords.val().trim() : '');
            var body = ((typeof $inBody != 'undefined') ? $inBody.val().trim() : '');

            if (!title.length || !keyWords.length || !body.length){
                GMessages.warning("Не все поля заполнены!");
                return ;
            }

            // Создание формв для отправки данных на сервер
            var data = new FormData();

            data.append('post_id',postId);
            data.append('title',title);
            data.append('keyWords',keyWords);
            data.append('body',body);

            // Отправка даных для записи
            sendForm('/edit/save',data,function(r){
                if (typeof r.status != 'undefined'){
                    if (r.status == 'SUCCESS'){
                        if (r.hasOwnProperty('postId')){
                            //window.location.origin.
                            window.location = '/index/view?post='+r['postId'];
                            // console.log(r['postId']['$id']);
                        } else {
                            GMessages.info("Изминения сохранены! Спотрите список!");
                        }
                    } else if (r.status == 'ERROR' && r.hasOwnProperty('message')){
                        GMessages.warning(r['message']);
                    } else {
                        GMessages.warning('Не удалось создать пост!');
                    }

                } else {console.log(r);}
            });
        });
});


// Удаление поста
$(function () {
    var $form = $('.js-delete-post');
    if (!$form.length){
        return ;
    }

    var $btnSubmit = $form.find('.js-submit');
    var $inPostId = $form.find('.js-post_id');

    // Кнопка отправки данных на сервер
    $btnSubmit
        .off('click')
        .on('click',function(){
            var postId = ((typeof $inPostId != 'undefined') ? $inPostId.val().trim() : '');

            // Создание формв для отправки данных на сервер
            var data = new FormData();

            data.append('post_id',postId);

            // Отправка даных для записи
            sendForm('/edit/delete',data,function(r){
                if (typeof r.status != 'undefined'){
                    if (r.status == 'SUCCESS'){
                        window.location = '/';
                    } else if (r.status == 'ERROR' && r.hasOwnProperty('message')){
                        GMessages.warning(r['message']);
                    } else {
                        GMessages.warning('Не удалось удалить пост!');
                    }

                } else {console.log(r);}
            });
        });

});
